import { createStackNavigator } from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
import ErrorScreen from './containers /error';
import homeScreen from './containers /homeScreen'
const navigator = createStackNavigator(
  {
    Home : homeScreen,
    Error: ErrorScreen,
  },
  {
      initialRouteName: 'Home',
      defaultNavigationOptions: {
      headerStyle:{backgroundColor:'rgb(40, 77, 120)',},
      headerTitleStyle:{fontSize:30,fontWeight:'bold',alignSelf:'center',color:"#a7bdcc"}
    },
  }
);

export default createAppContainer(navigator);

