import React from 'react';
import {StyleSheet} from 'react-native';
;
import { createStore,applyMiddleware,compose, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import reducer from '../reducers/reducer';
import WeatherApp from './WeatherApp';

const masterReducer = combineReducers({
    name:reducer,
  })
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const store = createStore(masterReducer,{name:["0","0","0","0","0"]},composeEnhancers(applyMiddleware(thunk)));

function homeScreen () {
return (
    <Provider store={store}>
    <WeatherApp/>
    </Provider>
);
}

  const styles=StyleSheet.create({

  })

  
  export default homeScreen;

  
