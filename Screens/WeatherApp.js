import React, { useState } from 'react';
import {Text,View,TextInput, SafeAreaView, Button,StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {anotherName} from '../actions/action';
import ErrorScreen from './error';
import { ScrollView } from 'react-native-gesture-handler';
function WeatherApp (props) {
const [Tt,changeText]=useState('Patna'); 

const dayArr=['Sun','Mon','Tue','Wed','Thurs','Fri','Sat'];
  var d = new Date();  
  // var day =    d.getDay();
  var day=4;
  let content=null;
  if(props.name==='error'){
    content = <ErrorScreen/>
  }
  else{
    content= props.name.map((data,i)=>{
      return (
        <View key={i} style={styles.temp}>
        <Text key = {i} style={styles.textStyle}>{dayArr[(day+i<7)? i+day : i-day +1 ]}     {data} °c</Text>
        </View>
      )
      // <Text key = {i} style={styles.temp}> temp:{data} c</Text>
    })
  }
return (
  
    <SafeAreaView>
     <View style={styles.mainContainer}>
        <Text style={styles.heading}>Weather forecast</Text>
        <TextInput 
        style={styles.input} 
        placeholder="ENTER CITY"
        onChangeText={(text)=>changeText(text)}
        
        /> 
        <Button 
        style={styles.btn} 
        title="search" 
       
        onPress={()=>{props.changeName(Tt)}}/>
          <Text style={styles.headcity}>{Tt}</Text>
        
          {content}
          
        {/* <Text>{props.changeName(Tt)}</Text> */}
    
      
       
    </View>
    </SafeAreaView>
);
}


const mapStateToProps = (state) => {
    return state;
  }
  const mapDispatchProps =(dispatch)=>{
    return {
      changeName:(name)=>{ dispatch(anotherName(name))}
    }
  }
  const styles=StyleSheet.create({
    heading:{
      fontSize:30,
      fontWeight:'bold',
      alignSelf:'center',
      marginTop:20,
    }, 
    input:{
        height:90,
        marginTop:30,
        borderColor:'black',
        borderWidth:3,
        fontSize:30,
     },
     btn:{
        height:50,
        width:50
     },
     textStyle:{
         fontSize:35,
         alignSelf:'center',
         borderBottomWidth:3,
     },
     temp:{
      height:50,
      borderColor:'black',
      borderWidth:3,
      marginTop:10,
      borderRadius:10,
     },
     headcity:{
         fontSize:40,
         color:'#35b599',
         alignSelf:'center',
     },
     mainContainer:{
      backgroundColor:'#fae6af',
      height:900,
     }

 
  })

  
  export default connect(mapStateToProps,mapDispatchProps)(WeatherApp);

  
