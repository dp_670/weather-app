import React, { Component } from 'react';
import {View,Text, StyleSheet} from 'react-native';

function ErrorScreen (){
    return(
        <View>
            <Text style={styles.text}>Something went wrong</Text>
        </View> 
    );
}
const styles=StyleSheet.create({
    text:{
        fontSize:30,
        color:'red',
        alignSelf:'center',
    }
}
)
export default ErrorScreen;