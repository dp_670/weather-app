
export const anotherName = (name) => {
    return async (dispatch)=>{
        try{
            const data = await fetch(`http://api.openweathermap.org/data/2.5/forecast?q=${name}&units=metric&appid=b5b23f6509dd1bc89b478e4dffe52b6f`)
            const res2= await data.json();
            const arr =[res2.list[0].main.temp,
            res2.list[8].main.temp,
            res2.list[16].main.temp,
            res2.list[24].main.temp,
            res2.list[32].main.temp]
            dispatch({type:'CHANGE_NAME', payload : arr})
            
        }
        catch(err){
            dispatch({type:'FRONT_ERR', payload : 'error'})
       }
    }
    }
    
